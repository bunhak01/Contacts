package com.example.bunha.mycontact;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nitya on 8/20/2017.
 */

public class FavAdapter extends RecyclerView.Adapter<FavAdapter.PersonViewHolder> {
    List<Person> personList;
    Context context;

    public FavAdapter(Context context) {
        this.personList = new ArrayList<>();
        this.context=context;
    }

    @Override
    public FavAdapter.PersonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_custom_layout, parent, false);
        return new FavAdapter.PersonViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FavAdapter.PersonViewHolder holder, int position) {
        Person p = personList.get(position);
        holder.tvName.setText(p.getName());
        //holder.tvName.setText(p.getNumber1());
    }

    @Override
    public int getItemCount() {
        return personList.size();
    }

    public void addPerson(List<Person> list){
        this.personList.addAll(list);
        notifyDataSetChanged();
    }

    public Person getPerson(int index){
        return this.personList.get(index);
    }

    public void removePerson(int pos){
        this.personList.remove(pos);
        notifyItemRemoved(pos);
    }

    public void clear(){
        this.personList.clear();
        notifyDataSetChanged();
    }
    public void noti(){
        notifyDataSetChanged();
    }

    public class PersonViewHolder extends RecyclerView.ViewHolder  {

        TextView tvName;

        public PersonViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i=new Intent(context,EditActivity.class);
                    i.putExtra("pos",personList.get(getAdapterPosition()).getId());
                    context.startActivity(i);
                }
            });
        }

    }
}
