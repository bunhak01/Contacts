package com.example.bunha.mycontact;

/**
 * Created by bunha on 8/10/2017.
 */

public class DbConstant {

    public static final String DB_NAME = "contact_db";
    public static final int DB_VERSION = 1;

    public static final String TABLE_PERSON = "tbl_person";
    public static final String PERSON_NAME = "person_name";
    public static final String PERSON_ID = "person_id";
    public static final String PERSON_NUMBER1 = "person_number1";
    public static final String PERSON_NUMBER2 = "person_number2";

    public static final String CREATE_TABLE_PERSON = "CREATE TABLE " +
            TABLE_PERSON + "(" +
            PERSON_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            PERSON_NAME + " TEXT NOT NULL," +
            PERSON_NUMBER1 + " TEXT," +
            PERSON_NUMBER2 + " TEXT)";

}
