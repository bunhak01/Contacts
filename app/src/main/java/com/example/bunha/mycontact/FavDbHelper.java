package com.example.bunha.mycontact;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Nitya on 9/5/2017.
 */

public class FavDbHelper {
    DbOpenHelper dbOpenHelper;

    public FavDbHelper(Context context) {
        dbOpenHelper = DbOpenHelper.newInstance(context);
    }

    public List<Person> findAllPersons() {

        List<Person> favList = new ArrayList<>();

        SQLiteDatabase db = dbOpenHelper.getReadableDatabase();
        Cursor cursor = db.query(
                DbConstant.TABLE_PERSON,
                new String[]{DbConstant.PERSON_ID, DbConstant.PERSON_NAME, DbConstant.PERSON_NUMBER1,DbConstant.PERSON_NUMBER2},
                null,
                null,
                null,
                null,
                null
        );

        try {
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    while (!cursor.isAfterLast()) {
                        Person p = Person.getPerson(cursor);
                        if (p != null) {
                            favList.add(p);
                        }
                        cursor.moveToNext();
                    }
                    return favList;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) cursor.close();
        }
        return Collections.emptyList();
    }

    public boolean addPerson(Person p) {
        SQLiteDatabase db = dbOpenHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DbConstant.PERSON_NAME, p.getName());
        contentValues.put(DbConstant.PERSON_NUMBER1, p.getNumber1());
        contentValues.put(DbConstant.PERSON_NUMBER2, p.getNumber2());

        long result = db.insert(DbConstant.TABLE_PERSON, null, contentValues);
        if (result > 0) {
            return true;
        }
        return false;
    }

    public boolean deletePerson(int id) {
        SQLiteDatabase db = dbOpenHelper.getWritableDatabase();
        long result = db.delete(DbConstant.TABLE_PERSON, DbConstant.PERSON_ID + " = ?", new String[]{String.valueOf(id)});
        if (result > 0) {
            return true;
        }
        return false;
    }

    public boolean updatePerson(Person p) {
        SQLiteDatabase db = dbOpenHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DbConstant.PERSON_NAME, p.getName());
        contentValues.put(DbConstant.PERSON_NUMBER1, p.getNumber1());
        contentValues.put(DbConstant.PERSON_NUMBER2, p.getNumber2());

        int result = db.update(
                DbConstant.TABLE_PERSON,
                contentValues, DbConstant.PERSON_ID+" = ?",
                new String[]{String.valueOf(p.getId())}
        );

        if (result > 0) {
            return true;
        }
        return false;
    }


    public List<Person> findPersonByName(String name) {
        SQLiteDatabase db = dbOpenHelper.getReadableDatabase();
        List<Person> personList = new ArrayList<>();
        Cursor cursor = db.rawQuery(
                "SELECT * FROM " + DbConstant.TABLE_PERSON + " WHERE " + DbConstant.PERSON_NAME + " LIKE ?",
                new String[]{"%" + name + "%"});

        try {
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    while (!cursor.isAfterLast()) {
                        Person p = Person.getPerson(cursor);
                        if (p != null) {
                            personList.add(p);
                        }
                        cursor.moveToNext();
                    }
                    return personList;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) cursor.close();
        }
        return Collections.emptyList();
    }
    public Person findPersonById(int id) {
        SQLiteDatabase db = dbOpenHelper.getReadableDatabase();
        Person p=new Person();
        Cursor cursor = db.rawQuery(
                "SELECT * FROM " + DbConstant.TABLE_PERSON + " WHERE " + DbConstant.PERSON_ID + " =? ",
                new String[]{  String.valueOf(id) });

        try {
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    while (!cursor.isAfterLast()) {
                        p = Person.getPerson(cursor);
                        if (p != null) {
                            return  p;
                        }
                        cursor.moveToNext();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) cursor.close();
        }
        return p;
    }
}
