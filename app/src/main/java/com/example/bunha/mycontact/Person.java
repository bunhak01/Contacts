package com.example.bunha.mycontact;

import android.database.Cursor;

/**
 * Created by bunha on 8/10/2017.
 */

public class Person {
    int id;
    String name;
    String number1;
    String number2;
    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", number1='" + number1 + '\'' +
                ", number2='" + number2 + '\'' +
                '}';
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Person(){

    }

    public Person(int id, String name, String number1, String number2) {
        this.id=id;
        this.name = name;
        this.number1 = number1;
        this.number2 = number2;
    }
    public Person(String name, String number1, String number2) {
        this.name = name;
        this.number1 = number1;
        this.number2 = number2;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber1() {
        return number1;
    }

    public void setNumber1(String number1) {
        this.number1 = number1;
    }

    public String getNumber2() {
        return number2;
    }

    public void setNumber2(String number2) {
        this.number2 = number2;
    }

    public static Person getPerson(Cursor cursor){

        if (cursor != null){
            int id = cursor.getInt(cursor.getColumnIndex(DbConstant.PERSON_ID));
            String name = cursor.getString(cursor.getColumnIndex(DbConstant.PERSON_NAME));
            String number1 = cursor.getString(cursor.getColumnIndex(DbConstant.PERSON_NUMBER1));
            String number2 = cursor.getString(cursor.getColumnIndex(DbConstant.PERSON_NUMBER2));
            return new Person(id, name, number1,number2);
        }
        return null;
    }

}
